#!/usr/bin/env python3
# -*- coding: utf-8 -*
##################################
## Author1: Eunchong Kim (eunchong.kim at cern.ch)
## Copyright: Copyright 2019, ldbtools
## Date: Aug. 2019
## Project: Local Database Tools
## Description: Plot data
##################################

PAGE_NAME = "plot_data"

from configs.development import *

import os
import sys
sys.path.append( os.path.dirname(os.path.dirname(os.path.abspath(__file__)) ) )
from scripts.src.func import *

plot_data_api = Blueprint('plot_data_api', __name__)

@plot_data_api.route("/plotData", methods=['GET', 'POST'])
def plotData():
    testRunId = request.args.get("testRunId")
    mapType = request.args.get("mapType")

    localdb = init().localdb
    componentTestRun_doc = localdb.componentTestRun.find_one({"testRun": testRunId})
    fs = gridfs.GridFS(localdb)
    if not componentTestRun_doc:
        return render_template("plotly.html")

    # Get chip SN
    name = componentTestRun_doc["name"]

    # Get created datetime
    # Auto-detect zones:
    from_zone = dateutil.tz.tzutc()
    to_zone = dateutil.tz.tzlocal()
    # Convert time zone
    created_datetime_utc = componentTestRun_doc["sys"]["mts"].replace(tzinfo=from_zone)
    created_datetime_local = created_datetime_utc.astimezone(to_zone)

    # Get data
    for attachment in componentTestRun_doc["attachments"]:
        if mapType in attachment["title"]:
            data = fs.get(ObjectId(attachment["code"])).read()
            data = data.decode("utf-8") 
            logging.debug(data)
            lines = data.split("\n")

            # data
            logging.debug(lines[0])
            if lines[0] == "Histo1d":
                logging.debug(line[0])

            if lines[0] == "Histo2d":
                return render_template("plotly.html")

            xaxis_title = lines[2]
            yaxis_title = lines[3]
            zaxis_title = lines[4]

            xbins_start = float(lines[5].split(" ")[1])
            xbins_end = float(lines[5].split(" ")[2])
            nbinsx = int(lines[5].split(" ")[0])
            xbins_size = (xbins_end - xbins_start)/nbinsx
            w_str_list = lines[7].split(" ")
            x = []
            w = []
            x_0 = xbins_start
            for w_str in w_str_list:
                if w_str == "": continue
                x.append(x_0)
                w.append(float(w_str))
                x_0 += xbins_size
            break

    graph = {
            "data": [
                {
                    "x": x,
                    "y": w,
                    "type": "bar"
                }
            ]
    }
    layout = {
            "title": name + "_" + mapType + ", " + created_datetime_local.strftime("%Y/%m/%d, %H:%M:%S"),
            "titlefont": {"size": 18},
            "xaxis": {
                "title": xaxis_title,
                "titlefont": {"size": 18},
                "tickfont": {"size": 18}
            },
            "yaxis": {
                "title": yaxis_title,
                "titlefont": {"size": 18},
                "tickfont": {"size": 18}
            },
            "legend": {
                "font": {"size": 18}
            }
    }

    graph_json = json.dumps(graph, cls=plotly.utils.PlotlyJSONEncoder)
    layout_json = json.dumps(layout, cls=plotly.utils.PlotlyJSONEncoder)

    # Only work online mode
    #figure = Figure(data=graph, layout=layout)
    #plotly.plotly.image.save_as(figure, filename='tmp/'+city, format='jpeg')

    return render_template("plotly.html", graph_json=graph_json, layout_json=layout_json)
