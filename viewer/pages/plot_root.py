### import 
import os, pwd, sys, glob, json, base64, io, subprocess
from pymongo               import MongoClient, DESCENDING, errors  # use mongodb scheme
from PIL                   import Image
from bson.objectid         import ObjectId  # handle bson format
import gridfs # gridfs system 

TMP_DIR       = '/var/tmp/{}'.format(pwd.getpwuid(os.geteuid()).pw_name) 
CACHE_DIR     = '{}/cache'.format(TMP_DIR)
_EXTENSIONS = ['png', 'jpeg', 'jpg', 'JPEG', 'jpe', 'jfif', 'pjpeg', 'pjp', 'gif']

def bin2image( typ, binary ):
    if typ in _EXTENSIONS:
        data = 'data:image/png;base64,' + binary
    if typ == 'pdf':
        filePdf = open( '{}/image.pdf'.format( TMP_DIR ), 'wb' )
        binData = a2b_base64( binary )
        filePdf.write( binData )
        filePdf.close()
        path = '{}/image.pdf'.format( TMP_DIR )
        image = convert_from_path( path )
        image[0].save( '{}/image.png'.format( TMP_DIR ), 'png' )
        binaryPng = open( '{}/image.png'.format( TMP_DIR ), 'rb' )
        byte = base64.b64encode( binaryPng.read() ).decode()
        binaryPng.close()
        data = 'data:image/png;base64,' + byte
    return data

def makeDir():
    if not os.path.isdir(TMP_DIR): 
        os.mkdir(TMP_DIR)
    user_dir = TMP_DIR + '/' + str(session.get('uuid','localuser'))
    if not os.path.isdir(user_dir): 
        os.mkdir(user_dir)
    for dir_ in _DIRS:
        if not os.path.isdir(dir_): 
            os.mkdir(dir_)

def cleanDir(dir_name):
    if os.path.isdir(dir_name): 
        shutil.rmtree(dir_name)
    os.mkdir(dir_name)

def retrieveFiles(localdb, run_oid):

    fs = gridfs.GridFS(localdb)
    query = { '_id': ObjectId(run_oid) }
    this_run = localdb.testRun.find_one(query)
    if not this_run: return

    run_dir = '{0}/{1}'.format(CACHE_DIR,run_oid)
    if os.path.isdir(run_dir):
        return
    else:
        os.makedirs(run_dir)

    #scanLog
    scan_log = {
        'connectivity': [],
        'testType'    : this_run['testType'],
        'targetCharge': this_run['targetCharge'],
        'targetTot'   : this_run['targetTot']
    }

    #scanCfg
    if this_run.get('scanCfg', '...')=='...': return False
    query = { '_id': ObjectId(this_run['scanCfg']) }
    this_config = localdb.config.find_one(query)
    file_path = '{0}/{1}.json'.format(run_dir, this_run['testType'])
    f = open(file_path, 'wb')
    f.write(fs.get(ObjectId(this_config['data_id'])).read())
    f.close()
   
    #data files
    query = { 'testRun': run_oid }
    entries = localdb.componentTestRun.find(query)
    ctr_oids = []
    for entry in entries:
        ctr_oids.append(str(entry['_id']))
    connectivity = {
        'chipType': this_run['chipType'],
        'chips'   : []
    }
    for ctr_oid in ctr_oids:
        query = { '_id': ObjectId(ctr_oid) }
        this_ctr = localdb.componentTestRun.find_one(query)
        if this_ctr.get('enable',1)==0: continue
        connectivity['chips'].append({
            'config'  : '{}.json'.format(this_ctr['name'])
        })
        for key in this_ctr:
            if not 'Cfg' in key or not this_run.get(key, '...')=='...': continue 
            query = { '_id': ObjectId(this_ctr[key]) }
            this_config = localdb.config.find_one(query)
            file_path = '{0}/{1}.json.{2}'.format(run_dir, this_ctr['name'], key[:-3])
            f = open(file_path, 'wb')
            f.write(fs.get(ObjectId(this_config['data_id'])).read())
            f.close()
        for data in this_ctr.get('attachments',[]):
            query = { '_id': ObjectId(data['code']) }
            file_path = '{0}/{1}_{2}'.format(run_dir, this_ctr['name'], data['filename'])
            f = open(file_path, 'wb')
            f.write(fs.get(ObjectId(data['code'])).read())
            f.close()
    scan_log['connectivity'].append(connectivity)
    
    file_path = '{0}/scanLog.json'.format(run_dir)
    f = open(file_path, 'w')
    json.dump( scan_log, f, indent=4 )
    f.close()
    
    return True

def plotRoot(plot_tool, run_oid):
    run_dir = '{0}/{1}'.format(CACHE_DIR,run_oid)
    if not os.path.isdir(run_dir): return

    thum_dir = '{}/thumbnail'.format(run_dir)
    if os.path.isdir(thum_dir):
        if not os.listdir(thum_dir)==[]: return
    else: os.mkdir('{}/thumbnail'.format(run_dir))

    #user_dir = '{0}/{1}/plot'.format(TMP_DIR, session.get('uuid','localuser'))
    #if not os.path.isdir(user_dir):
    #    os.makedirs(user_dir)

    command = [ '{}/plotWithRoot'.format(plot_tool), '-i', run_dir, '-B', '-e', 'png', '-p', '{}/parameters.json'.format(plot_tool)]
    subprocess.call(command)

    for filename in os.listdir(run_dir):
        if not 'png' in filename: continue
        filepath = '{0}/{1}'.format(run_dir, filename)
        image = Image.open(filepath)
        image.thumbnail((int(image.width/4), int(image.height/4)))
        thumbnail = '{0}/thumbnail/{1}'.format(run_dir, filename)
        image.save(thumbnail)

def setRootResult(localdb, run_oid, col, chip_oid=None):
    thum_dir = '{0}/{1}/thumbnail'.format(CACHE_DIR,run_oid)
    if not os.path.isdir(thum_dir): return

    chip_name = None
    plots = {}
    filepath = '{0}/{1}/plotLog.json'.format(CACHE_DIR,run_oid)
    with open(filepath, 'r') as f: plotLog = json.load(f)
    if chip_oid:
        query = { 'testRun': run_oid, col: chip_oid }
        this_ctr = localdb.componentTestRun.find_one(query)
        chip_name = this_ctr['name']
    for name in plotLog['chips']:
        if chip_name and not chip_name==name: continue
        for plotname in plotLog['chips'][name]:
            plots.update({ 
                plotname: { 'plot': [] }
            })
            for filename in plotLog['chips'][name][plotname]:
                filepath = '{0}/{1}_root.png'.format(thum_dir,filename)
                binary_image = open(filepath, 'rb')
                code_base64 = base64.b64encode(binary_image.read()).decode()
                binary_image.close()
                plots[plotname]['plot'].append({
                    'name': filename,
                    'url' : bin2image('png', code_base64)
                })
            plots[plotname]['num'] = len(plots[plotname]['plot'])
    return plots
