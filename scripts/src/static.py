from flask import Blueprint
import os, pwd

USER = pwd.getpwuid( os.geteuid() ).pw_name

app = Blueprint("upload", __name__,
    static_url_path='/var/tmp/{}/static'.format( USER ), static_folder='/var/tmp/{}/static'.format( USER )
)

app = Blueprint("result", __name__,
    static_url_path='/var/tmp/{}/result'.format( USER ), static_folder='/var/tmp/{}/result'.format( USER )
)

app = Blueprint("thumbnail", __name__,
    static_url_path='/var/tmp/{}/thumbnail'.format( USER ), static_folder='/var/tmp/{}/thumbnail'.format( USER )
)
